print("Aguarde, carregando PyCam...")
import matplotlib
matplotlib.use('Agg')
from PySide.QtGui import *
from PySide.QtCore import *
from PySide import QtCore, QtGui
import sys
import datetime
from ui_files import mainDialogV3 as mainDialog
import cv2
import csv
import numpy as np
from matplotlib import pyplot as plt
import time
import copy

__appname__ = "PyCam"
__version__ = "0.1"
__module__ = "main"


class MySignal(QObject):
    sig = Signal(str)
    visibilidade = Signal(float)


class ShowThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.tempo = 30
        self.rodar = False
        self.signal = MySignal()

    def run(self):
        while self.rodar:
            time.sleep(0.001 * self.tempo)
            self.signal.sig.emit('OK')


class GraphThread(QThread):
    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.tempo = 80
        self.rodar = False
        self.dadosX = []
        self.dadosY = []
        self.signal = MySignal()

    def run(self):
        while self.rodar:
            if self.dadosY:
                plt.figure(figsize=(5.5, 0.9))
                if len(self.dadosX) < 30:
                    plt.plot(self.dadosX, self.dadosY, '.-')
                    plt.axis([min(self.dadosX), max(self.dadosX) + 1, min(self.dadosY), max(self.dadosY) + 1])
                else:
                    plt.axis([min(self.dadosX), max(self.dadosX) + 1,
                              min(self.dadosY), max(self.dadosY) + 1])
                    vis = (np.max(self.dadosY) - np.min(self.dadosY)) / (np.max(self.dadosY) + np.min(self.dadosY))
                    plt.plot(self.dadosX, self.dadosY, '.-', label='vis={:1.2g}'.format(vis))
                    self.signal.visibilidade.emit(vis)
                    #plt.legend()
                #plt.ylabel('intensidade media')
                if self.rodar:
                    plt.xticks([])
                    plt.tight_layout()
                    plt.savefig('graph.png', format='png')
                # plt.show()
                plt.clf()
                plt.close()
            time.sleep(0.001 * self.tempo)


class MainDialog(QDialog, mainDialog.Ui_Dialog):
    medir = False
    marcar = False
    #leitura = []
    leitura = []
    ponto = []
    tempo = []
    pico = []
    marca = []
    contagem = 0
    franjas = 0
    descontinuidade_atual = 0
    descontinuidade_anterior = 0

    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)

        self.setWindowTitle(__appname__ + __version__)

        self.video_size = QSize(400, 400)
        self.verifica_cams()
        #self.setup_camera()
        self.sliderX.setValue(self.video_size.height() / 2)
        self.sliderY.setValue(self.video_size.width() / 2)
        self.ajusta_valor_slide()
        self.conectar_button.setFocus()
        self.cam.setCurrentIndex(1)
        self.lcd_franjas.display('----')
        self.image_label.setText("Câmera não conectada")

        self.timer = QTimer()
        self.timer.timeout.connect(self.display_video_stream)

        self.timer2 = QTimer()
        self.timer2.timeout.connect(self.salva_grafico)
        self.timer2.start(100)

        self.show_cam_thread = ShowThread()
        self.show_cam_thread.signal.sig.connect(self.mostrar_cam)

        self.graph_thread = GraphThread()
        self.graph_thread.signal.visibilidade.connect(self.lcd_visibilidade)

        self.quit_button.clicked.connect(self.close)
        self.sliderD.sliderMoved.connect(self.ajusta_valor_slide)
        self.run_button.clicked.connect(self.iniciar_medida)
        self.stop_button.clicked.connect(self.parar_medida)
        self.save_button.clicked.connect(self.salvando_arquivo)
        self.conectar_button.clicked.connect(self.setup_camera)
        self.marcar_button.clicked.connect(self.adicionar_marca)

        self.ganho_dial.valueChanged.connect(self.set_prop_dial)
        self.exposicao_dial.valueChanged.connect(self.set_prop_dial)
        self.brilho_dial.valueChanged.connect(self.set_prop_dial)
        self.contraste_dial.valueChanged.connect(self.set_prop_dial)

        self.ganho_spin.valueChanged.connect(self.set_prop_spin)
        self.exposicao_spin.valueChanged.connect(self.set_prop_spin)
        self.brilho_spin.valueChanged.connect(self.set_prop_spin)
        self.contraste_spin.valueChanged.connect(self.set_prop_spin)

    def adicionar_marca(self):
        if self.medir:
            self.marcar = True

    def lcd_visibilidade(self, data):
        self.lcd_vis.display("{0:.4f}".format(data))

    def set_prop_dial(self):
        ganho = self.ganho_dial.value()
        self.ganho_spin.setValue(int(ganho))

        brilho = self.brilho_dial.value()
        self.brilho_spin.setValue(int(brilho))

        expo = self.exposicao_dial.value()
        self.exposicao_spin.setValue(int(expo))

        contraste = self.contraste_dial.value()
        self.contraste_spin.setValue(int(contraste))

        if self.show_cam_thread.rodar:
            self.capture.set(cv2.CAP_PROP_CONTRAST, int(contraste))
            self.capture.set(cv2.CAP_PROP_EXPOSURE, int(expo))
            self.capture.set(cv2.CAP_PROP_BRIGHTNESS, int(brilho))
            self.capture.set(cv2.CAP_PROP_GAIN, int(ganho))

    def set_prop_spin(self):
        ganho = self.ganho_spin.value()
        self.ganho_dial.setValue(int(ganho))

        brilho = self.brilho_spin.value()
        self.brilho_dial.setValue(int(brilho))

        expo = self.exposicao_spin.value()
        self.exposicao_dial.setValue(int(expo))

        contraste = self.contraste_spin.value()
        self.contraste_dial.setValue(int(contraste))

        if self.show_cam_thread.rodar:
            self.capture.set(cv2.CAP_PROP_CONTRAST, int(contraste))
            self.capture.set(cv2.CAP_PROP_GAIN, int(ganho))
            self.capture.set(cv2.CAP_PROP_BRIGHTNESS, int(brilho))
            self.capture.set(cv2.CAP_PROP_EXPOSURE, int(expo))

    def mostrar_cam(self):

        if self.medir and self.contagem > 5:
            self.pixmap = QPixmap("graph.png")
            tamanho = self.pixmap.size()
            self.grafico_label.setGeometry(QtCore.QRect(10, 480, tamanho.width(), tamanho.height()))
            self.grafico_label.setPixmap(self.pixmap)
        if self.show_cam_thread.rodar:
            self.pixmap2 = QPixmap("cam.jpg")
            self.image_label.setPixmap(self.pixmap2)

    def setup_camera(self):
        """Initialize camera.
        """
        if not self.show_cam_thread.rodar:
            self.capture = cv2.VideoCapture(self.cam.currentIndex())
            self.timer.start(30)
            self.set_prop_dial()
            self.show_cam_thread.rodar = True
            self.show_cam_thread.start()
            #self.capture.set(3, self.video_size.width())
            #self.capture.set(4, self.video_size.height())

    def display_video_stream(self):
        """Read frame from camera and repaint QLabel widget.
        """
        _, frame = self.capture.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.flip(frame, 1)
        a = int(self.sliderX.value())
        b = int(self.sliderY.value())
        d = float(self.sliderD.value())/2
        x = [int(a-d), int(a+d)]
        y = [int(b - d), int(b+d)]
        ponto_medida = frame[x[0]:x[1], y[0]:y[1]]
        medida = int(ponto_medida.mean()*100/255)
        show = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        cv2.rectangle(show, (x[0], y[0]), (x[1], y[1]), (0, 0, 255), 2)
        x0, y0 = 0, 0
        crop_show = show[y0:y0+self.video_size.width(), x0:x0+self.video_size.height()]
        cv2.imwrite('cam.jpg', crop_show)
        #image = qimage2ndarray.array2qimage(show)  # SOLUTION FOR MEMORY LEAK

        if self.medir:
            agora = datetime.datetime.now()
            self.lcd.display(medida)
            if self.contagem == 0:
                self.tempo.append(0)
            else:
                delta = agora - self.antes
                self.tempo.append(self.tempo[self.contagem-1] + int(delta.total_seconds() * 1000))

            self.leitura.append(medida)
            self.ponto.append(self.contagem)
            if self.marcar:
                self.marca.append(medida)
                self.marcar = False
            else:
                self.marca.append(0)

            #print(self.tempo[self.contagem] - self.tempo[self.contagem-1])
            #self.image_label.setPixmap(QPixmap.fromImage(image))

            if len(self.tempo) > 30:
                self.graph_thread.dadosX = copy.deepcopy(self.tempo[self.contagem - 30:])
                self.graph_thread.dadosY = copy.deepcopy(self.leitura[self.contagem - 30:])
            else:
                self.graph_thread.dadosX = copy.deepcopy(self.tempo)
                self.graph_thread.dadosY = copy.deepcopy(self.leitura)

            self.contagem += 1
            self.antes = agora
        else:
            self.lcd.display(medida)
            #self.image_label.setPixmap(QPixmap.fromImage(image))

    def ajusta_valor_slide(self):
        self.sliderX.setMinimum(self.sliderD.value()/2)
        self.sliderX.setMaximum(self.video_size.height()-self.sliderD.value()/2)
        self.sliderY.setMinimum(self.sliderD.value()/2)
        self.sliderY.setMaximum(self.video_size.width()-self.sliderD.value()/2)

    def iniciar_medida(self):
        if self.show_cam_thread.rodar:
            if not self.medir:
                if len(self.leitura) > 1:
                    result = QMessageBox.question(self, "Atenção", "Os dados anteriores não salvos serão apagados, deseja continuar?",
                                                  QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
                else:
                    result = QMessageBox.Yes

                if result == QMessageBox.Yes:
                    #QMessageBox.warning(self, "Atenção", "Ligue o sistema de movimentação do espelho", QMessageBox.Ok)
                    self.medir = True
                    del self.leitura[:]
                    del self.ponto[:]
                    del self.tempo[:]
                    del self.marca[:]
                    self.contagem = 0
                    self.franjas = 0
                    self.antes = 0
                    self.descontinuidade_atual = 0
                    self.descontinuidade_anterior = 0
                    self.graph_thread.rodar = True
                    self.graph_thread.start()
        else:
            QMessageBox.warning(self, "Atenção", "Conecte a câmera para iniar a medida", QMessageBox.Ok)

    def parar_medida(self):
        if self.medir:
            self.medir = False
            self.graph_thread.rodar = False

    def salva_grafico(self):
        if self.medir and self.contagem > 5:
            #self.graphicsView.plot(self.leitura)  # This is working

            """
            plt.figure(figsize=(4, 0.9))
            if self.contagem < 30:
                plt.plot(self.tempo, self.leitura, '.-')
                plt.axis([min(self.tempo), max(self.tempo)+1, min(self.leitura), max(self.leitura)+1])
            else:
                temp_x = self.tempo[self.contagem - 30:]
                temp_y = self.leitura[self.contagem - 30:]
                plt.axis([min(temp_x), max(temp_x)+1,
                          min(temp_y), max(temp_y)+1])

                vis = (np.max(temp_y) - np.min(temp_y))/(np.max(temp_y) + np.min(temp_y))
                plt.plot(temp_x, temp_y, '.-', label='vis={:1.2g}'.format(vis))
                plt.legend()
            # plt.ylabel('intensidade media')
            # plt.savefig('image.png', format='png')
            # plt.show()

            #plt.clf()
            plt.close()
            """

    def update_figura(self):
        self.pixmap = QPixmap("image.png")
        self.grafico_label.setPixmap(self.pixmap)

    def salvando_arquivo(self):
        dir = "."
        fileObj = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        now = datetime.datetime.now()

        if not fileObj[0] == '':
            with open(fileObj[0], 'w') as csvfile:
                fieldnames = ['ponto', 'tempo', 'leitura', 'marcação']
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                writer.writerow({'ponto': 'Arquivo contendo os pontos amostrais do interferÔmetro de Michelson',
                                 'leitura': '', 'tempo': '', 'marcação': ''})
                writer.writerow({'ponto': 'Data:', 'leitura': '{}\{}\{}'.format(now.day, now.month, now.year),
                                 'tempo': '', 'marcação': ''})
                writer.writerow({'ponto': 'Hora:', 'leitura': '{}:{}:{}'.format(now.hour, now.minute, now.second),
                                 'tempo': '', 'marcação': ''})
                writer.writerow({'ponto': 'Quantidade de franjas:', 'leitura': int(self.franjas), 'tempo': '',
                                 'marcação': ''})

                writer.writeheader()
                for linha in range(len(self.ponto)):
                    writer.writerow({'ponto': self.ponto[linha], 'leitura': self.leitura[linha],
                                     'tempo': self.tempo[linha], 'marcação': self.marca[linha]})

    def monitorar_descontinuidade(self, atual):
        if atual == 1:
            if self.descontinuidade_anterior == -1:
                self.franjas += 0.5
            self.descontinuidade_anterior = 1
        if atual == -1:
            if self.descontinuidade_anterior == 1:
                self.franjas += 0.5
            self.descontinuidade_anterior = -1
        self.lcd_franjas.display(int(self.franjas))

    def verifica_cams(self):
        cont = 0
        self.cam.clear()
        while True:
            teste = cv2.VideoCapture(cont)
            if not teste.isOpened():
                break
            #teste.release()
            self.cam.addItem("Câmera "+str(cont))
            cont += 1

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""

        result = QMessageBox.question(self, __appname__, "Tem certeza que deseja sair?", QMessageBox.Yes | QMessageBox.No,
                                      QMessageBox.Yes)
        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            self.graph_thread.rodar = False
            self.show_cam_thread.rodar = False
            time.sleep(0.1)
            self.timer.stop()
            self.timer2.stop()
            time.sleep(0.1)
            self.graph_thread.exit()
            self.show_cam_thread.exit()
            event.accept()
        else:
            event.ignore()

if __name__ == '__main__':
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("Lf25")
    QCoreApplication.setOrganizationDomain("sites.ifi.unicamp.br/labeletro")

    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()


