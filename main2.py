from PySide.QtGui import *
from PySide.QtCore import *
import sys
import datetime
from ui_files import mainDialogV2 as mainDialog
import qimage2ndarray
import cv2
import csv
import numpy as np
import matplotlib.pyplot as plt

__appname__="franjas"

class MainDialog(QDialog, mainDialog.Ui_Dialog):
    medir = 0
    #leitura = []
    leitura = np.zeros(120000, dtype=np.int8) # speed and memory management
    ponto = np.zeros(120000, dtype=np.int8)
    tempo = np.zeros(120000, dtype=np.int8)
    pico = []
    contagem = 0
    franjas = 0
    descontinuidade_atual = 0
    descontinuidade_anterior = 0
    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent)

        self.video_size = QSize(600, 600)
        self.setupUi(self)

        self.verifica_cams()
        #self.setup_camera()
        self.ajusta_valor_slide()
        self.conectar_button.setFocus()
        self.cam.setCurrentIndex(1)


        self.quit_button.clicked.connect(self.close)
        self.sliderD.sliderMoved.connect(self.ajusta_valor_slide)
        self.run_button.clicked.connect(self.iniciar_medida)
        self.stop_button.clicked.connect(self.parar_medida)
        self.save_button.clicked.connect(self.salvando_arquivo)
        self.conectar_button.clicked.connect(self.setup_camera)
        self.ganho_dial.valueChanged.connect(self.set_prop_dial)
        self.exposicao_dial.valueChanged.connect(self.set_prop_dial)
        self.brilho_dial.valueChanged.connect(self.set_prop_dial)
        self.contraste_dial.valueChanged.connect(self.set_prop_dial)

    def set_prop_dial(self):
        ganho = self.ganho_dial.value()
        self.ganho_spin.setValue(int(ganho))
        self.capture.set(cv2.CAP_PROP_GAIN, int(ganho))

        brilho = self.brilho_dial.value()
        self.brilho_spin.setValue(int(brilho))
        self.capture.set(cv2.CAP_PROP_BRIGHTNESS, int(brilho))

        expo = self.exposicao_dial.value()
        self.exposicao_spin.setValue(int(expo))
        self.capture.set(cv2.CAP_PROP_EXPOSURE, int(expo))

        contraste = self.contraste_dial.value()
        self.contraste_spin.setValue(int(contraste))
        self.capture.set(cv2.CAP_PROP_CONTRAST, int(contraste))


    def setup_camera(self):
        """Initialize camera.
        """
        self.capture = cv2.VideoCapture(self.cam.currentIndex())
        self.capture.set(3, self.video_size.width())
        self.capture.set(4, self.video_size.height())
        self.capture.set(cv2.CAP_PROP_ZOOM, 2)
        self.timer = QTimer()
        self.timer.timeout.connect(self.display_video_stream)
        self.timer.start(20)
        self.set_prop_dial()

    def display_video_stream(self):
        """Read frame from camera and repaint QLabel widget.
        """
        _, frame = self.capture.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.flip(frame, 1)
        a = int(self.sliderX.value())
        b = int(self.sliderY.value())
        d = float(self.sliderD.value())/2
        x = [int(a-d), int(a+d)]
        y = [int(b - d), int(b+d)]
        ponto_medida = frame[x[0]:x[1], y[0]:y[1]]
        medida = int(ponto_medida.mean()*100/255)
        show = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        cv2.rectangle(show, (x[0], y[0]), (x[1], y[1]), (255, 0, 0), 2)
        image = qimage2ndarray.array2qimage(show)  # SOLUTION FOR MEMORY LEAK

        if self.medir:
            tempo1 = datetime.datetime.now()
            self.tempo.append(tempo1)
            self.leitura.append(medida)
            self.ponto.append(self.contagem)
            self.image_label.setPixmap(QPixmap.fromImage(image))
            if self.contagem > 3:
                delta = self.tempo[self.contagem-1] - self.tempo[self.contagem-2]
                #print(int(delta.total_seconds() * 1000))]
                print(self.sliderY.value(),self.sliderX.value())
            if self.contagem % 5 == 0 and self.contagem > 2:
                self.salva_figura()
                self.update_figura()
                self.lcd.display(medida)
            if len(self.ponto) > 5000:
                atual = len(self.leitura) - 1
                gap = self.gap.value()
                sub1 = self.leitura[atual] - self.leitura[atual-1]
                sub2 = self.leitura[atual-1] - self.leitura[atual-2]
                sub3 = self.leitura[atual-2] - self.leitura[atual-3]
                if abs(sub1) > gap or abs(sub2) > gap or abs(sub3) > gap:
                    if sub1 < 0 and sub2 > 0 and sub3 > 0:
                        self.pico.append(1)
                        self.monitorar_descontinuidade(1)
                    elif sub1 < 0 and sub2 < 0 and sub3 > 0:
                        self.pico.append(1)
                        self.monitorar_descontinuidade(1)
                    elif sub1 < 0 and sub2 > 0 and sub3 < 0:
                        self.pico.append(1)
                        self.monitorar_descontinuidade(1)
                    elif sub1 > 0 and sub2 < 0 and sub3 < 0:
                        self.pico.append(-1)
                        self.monitorar_descontinuidade(-1)
                    elif sub1 > 0 and sub2 > 0 and sub3 < 0:
                        self.pico.append(-1)
                        self.monitorar_descontinuidade(-1)
                    elif sub1 > 0 and sub2 < 0 and sub3 > 0:
                        self.pico.append(-1)
                        self.monitorar_descontinuidade(-1)
                    else:
                        self.pico.append(0)
                else:
                    self.pico.append(0)

            else:
                self.pico.append(0)

        else:
            self.lcd.display(medida)
            self.image_label.setPixmap(QPixmap.fromImage(image))

        self.contagem += 1


    def ajusta_valor_slide(self):
        self.sliderX.setMinimum(self.sliderD.value()/2)
        self.sliderX.setMaximum(600-self.sliderD.value()/2)
        self.sliderY.setMinimum(self.sliderD.value()/2)
        self.sliderY.setMaximum(600-self.sliderD.value()/2)

    def iniciar_medida(self):
        if not self.medir:
            if len(self.leitura) > 1:
                result = QMessageBox.question(self, "Atenção", "Os dados anteriores não salvos serão apagados, deseja continuar?",
                                              QMessageBox.Yes | QMessageBox.No, QMessageBox.Yes)
            else:
                result = QMessageBox.Yes

            if result == QMessageBox.Yes:
                QMessageBox.warning(self, "Atenção", "Ligue o sistema de movimentação do espelho", QMessageBox.Ok)
                self.medir = 1
                del self.leitura[:]
                del self.ponto[:]
                self.contagem = 0
                self.franjas = 0
                self.descontinuidade_atual = 0
                self.descontinuidade_anterior = 0

    def parar_medida(self):
        if self.medir:
            self.medir = 0

    def salva_figura(self):
        plt.figure(figsize=(4, 0.9))
        if self.contagem < 30:
            plt.plot(self.ponto, self.leitura, '.-')
            plt.axis([min(self.ponto), max(self.ponto), min(self.leitura), max(self.leitura)])
        else:
            temp_x = self.ponto[self.contagem - 30:]
            temp_y = self.leitura[self.contagem - 30:]
            vis = (np.max(temp_y) - np.min(temp_y))/(np.max(temp_y) + np.min(temp_y))
            plt.plot(temp_x, temp_y, '.-',label='vis={:1.2g}'.format(vis))
            plt.axis([min(temp_x), max(temp_x),
                      min(temp_y), max(temp_y)])
            plt.legend()

        # plt.ylabel('intensidade media')
        plt.savefig('image.png', format='png')
        # plt.show()
        plt.clf()
        plt.close()

    def update_figura(self):
        self.pixmap = QPixmap("image.png")
        self.grafico_label.setPixmap(self.pixmap)

    def salvando_arquivo(self):
        dir = "."
        fileObj = QFileDialog.getSaveFileName(self, __appname__, dir=dir, filter="CSV Files (*.csv)")
        now = datetime.datetime.now()

        with open(fileObj[0], 'w') as csvfile:
            fieldnames = ['ponto', 'leitura', 'pico']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writerow({'ponto': 'Arquivo contendo os pontos amostrais do interferÔmetro de Michelson',
                             'leitura': '', 'pico': ''})
            writer.writerow({'ponto': 'Data:', 'leitura': '{}\{}\{}'.format(now.day, now.month, now.year),
                             'pico': ''})
            writer.writerow({'ponto': 'Hora:', 'leitura': '{}\{}\{}'.format(now.hour, now.minute, now.second),
                             'pico': ''})
            writer.writerow({'ponto': 'Quantidade de franjas:', 'leitura': int(self.franjas), 'pico': ''})

            writer.writeheader()
            for linha in range(len(self.ponto)):
                writer.writerow({'ponto': self.ponto[linha], 'leitura': self.leitura[linha], 'pico': self.pico[linha]})

    def monitorar_descontinuidade(self, atual):
        if atual == 1:
            if self.descontinuidade_anterior == -1:
                self.franjas += 0.5
            self.descontinuidade_anterior = 1
        if atual == -1:
            if self.descontinuidade_anterior == 1:
                self.franjas += 0.5
            self.descontinuidade_anterior = -1
        self.lcd_franjas.display(int(self.franjas))

    def verifica_cams(self):
        cont = 0
        self.cam.clear()
        while True:
            teste = cv2.VideoCapture(cont)
            if (teste.isOpened() == False):
                break
            #teste.release()
            self.cam.addItem("Câmera "+str(cont))
            cont += 1


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()


