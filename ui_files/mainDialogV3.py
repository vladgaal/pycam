# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainDialogV3.ui'
#
# Created: Wed Nov  1 15:25:12 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(730, 600)
        Dialog.setMinimumSize(QtCore.QSize(0, 0))
        Dialog.setMaximumSize(QtCore.QSize(16000, 16000))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/main/icons/franja.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("QDialog{\n"
"background-color: qlineargradient(spread:reflect, x1:0.5, y1:1, x2:0.5, y2:0, stop:0 rgba(90, 7, 90, 255), stop:0.372881 rgba(26, 23, 99, 252), stop:0.672316 rgba(26, 23, 99, 253), stop:1 rgba(0, 125, 125, 255));\n"
"}\n"
"\n"
"QLabel{\n"
"color: White;\n"
"}\n"
"\n"
"\n"
"QToolButton{\n"
"background-color: Transparent; \n"
"color: White;\n"
"border: 2px ridge White;\n"
"border-radius: 10px;\n"
"}\n"
"\n"
"QToolButton:hover{\n"
"background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, stop:0.215909 rgba(0, 0, 0, 0), stop:1 rgba(15, 243, 255, 255));\n"
"}\n"
"\n"
"QLCDNumber{\n"
"color: White;\n"
"}\n"
"\n"
"QSpinBox { border: 2px solid  white; \n"
"border-radius: 5px; \n"
"color: White;\n"
"background-color: transparent;\n"
" }\n"
"\n"
"QSpinBox::up-button { \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::up-button:pressed{ \n"
"background-color: white;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"\n"
"QSpinBox::down-button{ \n"
"background-color: Transparent;\n"
"width: 14px;\n"
"height: 10px; \n"
"}\n"
"QSpinBox::down-button:pressed{ background-color: white;width: 14px;height: 10px; }\n"
"\n"
"QSpinBox::up-arrow#ganho_spin{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#ganho_spin{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::up-arrow#ganho_spin:pressed{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#ganho_spin:pressed{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"\n"
"QSpinBox::up-arrow#brilho_spin{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#brilho_spin{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::up-arrow#brilho_spin:pressed{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#brilho_spin:pressed{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"\n"
"QSpinBox::up-arrow#contraste_spin{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#contraste_spin{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::up-arrow#contraste_spin:pressed{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#contraste_spin:pressed{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"\n"
"QSpinBox::up-arrow#exposicao_spin{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#exposicao_spin{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::up-arrow#exposicao_spin:pressed{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#exposicao_spin:pressed{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"\n"
"QSpinBox::up-arrow#gap{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#gap{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::up-arrow#gap:pressed{ background-color: transparent;border-left: 7px solid none;border-right: 7px solid none;border-bottom: 6px solid white;width: 0px; height: 0px; }\n"
"QSpinBox::down-arrow#gap:pressed{ border-left: 7px solid none;border-right: 7px solid none;border-top: 6px solid white;width: 0px; height: 0px; }\n"
"\n"
"\n"
"")
        self.image_label = QtGui.QLabel(Dialog)
        self.image_label.setGeometry(QtCore.QRect(50, 10, 400, 400))
        self.image_label.setFrameShape(QtGui.QFrame.Box)
        self.image_label.setText("")
        self.image_label.setAlignment(QtCore.Qt.AlignCenter)
        self.image_label.setObjectName("image_label")
        self.sliderX = QtGui.QSlider(Dialog)
        self.sliderX.setGeometry(QtCore.QRect(50, 420, 401, 20))
        self.sliderX.setMinimum(0)
        self.sliderX.setMaximum(800)
        self.sliderX.setProperty("value", 400)
        self.sliderX.setSliderPosition(400)
        self.sliderX.setOrientation(QtCore.Qt.Horizontal)
        self.sliderX.setObjectName("sliderX")
        self.sliderY = QtGui.QSlider(Dialog)
        self.sliderY.setGeometry(QtCore.QRect(20, 10, 20, 400))
        self.sliderY.setMaximum(800)
        self.sliderY.setProperty("value", 400)
        self.sliderY.setSliderPosition(400)
        self.sliderY.setOrientation(QtCore.Qt.Vertical)
        self.sliderY.setInvertedAppearance(True)
        self.sliderY.setObjectName("sliderY")
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(240, 440, 16, 16))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtGui.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(0, 190, 21, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setWordWrap(False)
        self.label_2.setObjectName("label_2")
        self.sliderD = QtGui.QSlider(Dialog)
        self.sliderD.setGeometry(QtCore.QRect(460, 10, 20, 400))
        self.sliderD.setMinimum(5)
        self.sliderD.setMaximum(100)
        self.sliderD.setPageStep(1)
        self.sliderD.setProperty("value", 50)
        self.sliderD.setSliderPosition(50)
        self.sliderD.setOrientation(QtCore.Qt.Vertical)
        self.sliderD.setObjectName("sliderD")
        self.label_3 = QtGui.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(480, 150, 21, 131))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.grafico_label = QtGui.QLabel(Dialog)
        self.grafico_label.setGeometry(QtCore.QRect(10, 480, 571, 91))
        self.grafico_label.setFrameShape(QtGui.QFrame.Box)
        self.grafico_label.setText("")
        self.grafico_label.setObjectName("grafico_label")
        self.quit_button = QtGui.QToolButton(Dialog)
        self.quit_button.setGeometry(QtCore.QRect(510, 430, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.quit_button.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/main/icons/Exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.quit_button.setIcon(icon1)
        self.quit_button.setIconSize(QtCore.QSize(32, 32))
        self.quit_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.quit_button.setObjectName("quit_button")
        self.save_button = QtGui.QToolButton(Dialog)
        self.save_button.setGeometry(QtCore.QRect(510, 390, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.save_button.setFont(font)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/main/icons/Save.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.save_button.setIcon(icon2)
        self.save_button.setIconSize(QtCore.QSize(32, 32))
        self.save_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.save_button.setObjectName("save_button")
        self.run_button = QtGui.QToolButton(Dialog)
        self.run_button.setGeometry(QtCore.QRect(510, 270, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.run_button.setFont(font)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/main/icons/Start.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.run_button.setIcon(icon3)
        self.run_button.setIconSize(QtCore.QSize(32, 32))
        self.run_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.run_button.setObjectName("run_button")
        self.label_5 = QtGui.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(510, 0, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.lcd = QtGui.QLCDNumber(Dialog)
        self.lcd.setGeometry(QtCore.QRect(510, 20, 111, 40))
        self.lcd.setObjectName("lcd")
        self.stop_button = QtGui.QToolButton(Dialog)
        self.stop_button.setGeometry(QtCore.QRect(510, 350, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.stop_button.setFont(font)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/main/icons/Stop.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stop_button.setIcon(icon4)
        self.stop_button.setIconSize(QtCore.QSize(32, 32))
        self.stop_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.stop_button.setObjectName("stop_button")
        self.lcd_franjas = QtGui.QLCDNumber(Dialog)
        self.lcd_franjas.setGeometry(QtCore.QRect(510, 80, 111, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lcd_franjas.setFont(font)
        self.lcd_franjas.setFrameShape(QtGui.QFrame.Box)
        self.lcd_franjas.setLineWidth(1)
        self.lcd_franjas.setMidLineWidth(0)
        self.lcd_franjas.setSmallDecimalPoint(False)
        self.lcd_franjas.setObjectName("lcd_franjas")
        self.label_6 = QtGui.QLabel(Dialog)
        self.label_6.setGeometry(QtCore.QRect(510, 60, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_6.setFont(font)
        self.label_6.setObjectName("label_6")
        self.gap = QtGui.QSpinBox(Dialog)
        self.gap.setGeometry(QtCore.QRect(580, 120, 41, 21))
        self.gap.setMinimum(5)
        self.gap.setSingleStep(5)
        self.gap.setProperty("value", 20)
        self.gap.setObjectName("gap")
        self.label_7 = QtGui.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(510, 120, 71, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")
        self.line = QtGui.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(7, 460, 711, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.line_2 = QtGui.QFrame(Dialog)
        self.line_2.setGeometry(QtCore.QRect(490, 10, 20, 451))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.line_3 = QtGui.QFrame(Dialog)
        self.line_3.setGeometry(QtCore.QRect(510, 140, 110, 20))
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.cam = QtGui.QComboBox(Dialog)
        self.cam.setGeometry(QtCore.QRect(510, 180, 111, 22))
        self.cam.setObjectName("cam")
        self.label_8 = QtGui.QLabel(Dialog)
        self.label_8.setGeometry(QtCore.QRect(510, 150, 71, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        self.line_4 = QtGui.QFrame(Dialog)
        self.line_4.setGeometry(QtCore.QRect(510, 250, 110, 20))
        self.line_4.setFrameShape(QtGui.QFrame.HLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.conectar_button = QtGui.QToolButton(Dialog)
        self.conectar_button.setGeometry(QtCore.QRect(510, 210, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.conectar_button.setFont(font)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap(":/main/icons/Camera.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.conectar_button.setIcon(icon5)
        self.conectar_button.setIconSize(QtCore.QSize(32, 32))
        self.conectar_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.conectar_button.setObjectName("conectar_button")
        self.line_5 = QtGui.QFrame(Dialog)
        self.line_5.setGeometry(QtCore.QRect(10, 570, 711, 20))
        self.line_5.setFrameShape(QtGui.QFrame.HLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.label_9 = QtGui.QLabel(Dialog)
        self.label_9.setGeometry(QtCore.QRect(10, 580, 421, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_9.setFont(font)
        self.label_9.setObjectName("label_9")
        self.brilho_dial = QtGui.QDial(Dialog)
        self.brilho_dial.setGeometry(QtCore.QRect(640, 30, 81, 71))
        self.brilho_dial.setMinimum(0)
        self.brilho_dial.setMaximum(255)
        self.brilho_dial.setSingleStep(1)
        self.brilho_dial.setProperty("value", 169)
        self.brilho_dial.setObjectName("brilho_dial")
        self.gain_label = QtGui.QLabel(Dialog)
        self.gain_label.setGeometry(QtCore.QRect(639, 20, 81, 20))
        self.gain_label.setAlignment(QtCore.Qt.AlignCenter)
        self.gain_label.setObjectName("gain_label")
        self.gain_label_2 = QtGui.QLabel(Dialog)
        self.gain_label_2.setGeometry(QtCore.QRect(640, 130, 81, 21))
        self.gain_label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.gain_label_2.setObjectName("gain_label_2")
        self.contraste_dial = QtGui.QDial(Dialog)
        self.contraste_dial.setGeometry(QtCore.QRect(640, 140, 81, 71))
        self.contraste_dial.setMinimum(0)
        self.contraste_dial.setMaximum(255)
        self.contraste_dial.setSingleStep(1)
        self.contraste_dial.setProperty("value", 169)
        self.contraste_dial.setObjectName("contraste_dial")
        self.gain_label_3 = QtGui.QLabel(Dialog)
        self.gain_label_3.setGeometry(QtCore.QRect(640, 240, 81, 20))
        self.gain_label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.gain_label_3.setObjectName("gain_label_3")
        self.ganho_dial = QtGui.QDial(Dialog)
        self.ganho_dial.setGeometry(QtCore.QRect(640, 250, 81, 71))
        self.ganho_dial.setMinimum(0)
        self.ganho_dial.setMaximum(255)
        self.ganho_dial.setSingleStep(1)
        self.ganho_dial.setObjectName("ganho_dial")
        self.gain_label_4 = QtGui.QLabel(Dialog)
        self.gain_label_4.setGeometry(QtCore.QRect(640, 350, 81, 20))
        self.gain_label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.gain_label_4.setObjectName("gain_label_4")
        self.exposicao_dial = QtGui.QDial(Dialog)
        self.exposicao_dial.setGeometry(QtCore.QRect(640, 360, 81, 71))
        self.exposicao_dial.setMinimum(0)
        self.exposicao_dial.setMaximum(255)
        self.exposicao_dial.setSingleStep(1)
        self.exposicao_dial.setObjectName("exposicao_dial")
        self.exposicao_spin = QtGui.QSpinBox(Dialog)
        self.exposicao_spin.setGeometry(QtCore.QRect(640, 430, 81, 22))
        self.exposicao_spin.setMaximum(255)
        self.exposicao_spin.setSingleStep(10)
        self.exposicao_spin.setObjectName("exposicao_spin")
        self.line_6 = QtGui.QFrame(Dialog)
        self.line_6.setGeometry(QtCore.QRect(620, 10, 20, 451))
        self.line_6.setFrameShape(QtGui.QFrame.VLine)
        self.line_6.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_6.setObjectName("line_6")
        self.ganho_spin = QtGui.QSpinBox(Dialog)
        self.ganho_spin.setGeometry(QtCore.QRect(640, 320, 81, 22))
        self.ganho_spin.setMaximum(255)
        self.ganho_spin.setSingleStep(10)
        self.ganho_spin.setObjectName("ganho_spin")
        self.contraste_spin = QtGui.QSpinBox(Dialog)
        self.contraste_spin.setGeometry(QtCore.QRect(640, 210, 81, 22))
        self.contraste_spin.setMaximum(255)
        self.contraste_spin.setSingleStep(10)
        self.contraste_spin.setProperty("value", 196)
        self.contraste_spin.setObjectName("contraste_spin")
        self.line_7 = QtGui.QFrame(Dialog)
        self.line_7.setGeometry(QtCore.QRect(639, 340, 81, 20))
        self.line_7.setFrameShape(QtGui.QFrame.HLine)
        self.line_7.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_7.setObjectName("line_7")
        self.line_8 = QtGui.QFrame(Dialog)
        self.line_8.setGeometry(QtCore.QRect(640, 230, 81, 20))
        self.line_8.setFrameShape(QtGui.QFrame.HLine)
        self.line_8.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_8.setObjectName("line_8")
        self.brilho_spin = QtGui.QSpinBox(Dialog)
        self.brilho_spin.setGeometry(QtCore.QRect(640, 100, 81, 21))
        self.brilho_spin.setMaximum(255)
        self.brilho_spin.setSingleStep(10)
        self.brilho_spin.setProperty("value", 196)
        self.brilho_spin.setObjectName("brilho_spin")
        self.line_9 = QtGui.QFrame(Dialog)
        self.line_9.setGeometry(QtCore.QRect(640, 120, 81, 20))
        self.line_9.setFrameShape(QtGui.QFrame.HLine)
        self.line_9.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_9.setObjectName("line_9")
        self.label_10 = QtGui.QLabel(Dialog)
        self.label_10.setGeometry(QtCore.QRect(600, 490, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_10.setFont(font)
        self.label_10.setObjectName("label_10")
        self.lcd_vis = QtGui.QLCDNumber(Dialog)
        self.lcd_vis.setGeometry(QtCore.QRect(600, 510, 111, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.lcd_vis.setFont(font)
        self.lcd_vis.setFrameShape(QtGui.QFrame.Box)
        self.lcd_vis.setLineWidth(1)
        self.lcd_vis.setMidLineWidth(0)
        self.lcd_vis.setSmallDecimalPoint(False)
        self.lcd_vis.setObjectName("lcd_vis")
        self.marcar_button = QtGui.QToolButton(Dialog)
        self.marcar_button.setGeometry(QtCore.QRect(510, 310, 111, 38))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.marcar_button.setFont(font)
        icon6 = QtGui.QIcon()
        icon6.addPixmap(QtGui.QPixmap(":/main/icons/pencil.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.marcar_button.setIcon(icon6)
        self.marcar_button.setIconSize(QtCore.QSize(32, 32))
        self.marcar_button.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        self.marcar_button.setObjectName("marcar_button")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", " X", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", " Y", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "L\n"
"a\n"
"r\n"
"g\n"
"u\n"
"r\n"
"a", None, QtGui.QApplication.UnicodeUTF8))
        self.quit_button.setText(QtGui.QApplication.translate("Dialog", "Sair", None, QtGui.QApplication.UnicodeUTF8))
        self.save_button.setText(QtGui.QApplication.translate("Dialog", "Salvar", None, QtGui.QApplication.UnicodeUTF8))
        self.run_button.setText(QtGui.QApplication.translate("Dialog", "Iniciar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Dialog", "Média de pixel:", None, QtGui.QApplication.UnicodeUTF8))
        self.stop_button.setText(QtGui.QApplication.translate("Dialog", "Parar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Dialog", "Franjas:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("Dialog", "Gap:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_8.setText(QtGui.QApplication.translate("Dialog", "Câmera:", None, QtGui.QApplication.UnicodeUTF8))
        self.conectar_button.setText(QtGui.QApplication.translate("Dialog", "Conectar", None, QtGui.QApplication.UnicodeUTF8))
        self.label_9.setText(QtGui.QApplication.translate("Dialog", "Escrito e compilado por Vladimir Gaal.", None, QtGui.QApplication.UnicodeUTF8))
        self.gain_label.setText(QtGui.QApplication.translate("Dialog", "Brilho", None, QtGui.QApplication.UnicodeUTF8))
        self.gain_label_2.setText(QtGui.QApplication.translate("Dialog", "Contraste", None, QtGui.QApplication.UnicodeUTF8))
        self.gain_label_3.setText(QtGui.QApplication.translate("Dialog", "Ganho", None, QtGui.QApplication.UnicodeUTF8))
        self.gain_label_4.setText(QtGui.QApplication.translate("Dialog", "Exposição", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(QtGui.QApplication.translate("Dialog", "Visibilidade:", None, QtGui.QApplication.UnicodeUTF8))
        self.marcar_button.setText(QtGui.QApplication.translate("Dialog", "Marcar", None, QtGui.QApplication.UnicodeUTF8))

import icones_rc
